import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static class Link {
        public int source;
        public int destination;
        public int distance;

        public Link(int source, int destination, int distance) {
            this.source = source;
            this.destination = destination;
            this.distance = distance;
        }
    }

    public static class Factory {
        public int entityId;
        public int owner;
        public int count;
        public int production;

        public Factory(int entityId, int owner, int count, int production) {
            this.entityId = entityId;
            this.owner = owner;
            this.count = count;
            this.production = production;
        }
    }

    public static class Troop {
        public int entityId;
        public int owner;
        public int source;
        public int target;
        public int count;
        public int turnCount;

        public Troop(int entityId, int owner, int source, int target, int count, int turnCount) {
            this.entityId = entityId;
            this.owner = owner;
            this.source = source;
            this.target = target;
            this.count = count;
            this.turnCount = turnCount;
        }
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int factoryCount = in.nextInt(); // the number of factories
        int linkCount = in.nextInt(); // the number of links between factories


        List<Link> links = new LinkedList<>();
        for (int i = 0; i < linkCount; i++) {
            int factory1 = in.nextInt();
            int factory2 = in.nextInt();
            int distance = in.nextInt();
            links.add(new Link(factory1,factory2,distance));
        }

        // game loop
        while (true) {
            List<Factory> factories = new LinkedList<>();
            List<Troop> troops = new LinkedList<>();
            int entityCount = in.nextInt(); // the number of entities (e.g. factories and troops)
            for (int i = 0; i < entityCount; i++) {
                int entityId = in.nextInt();
                String entityType = in.next();

                int arg1 = in.nextInt();
                int arg2 = in.nextInt();
                int arg3 = in.nextInt();
                int arg4 = in.nextInt();
                int arg5 = in.nextInt();
                switch (entityType){
                    case "FACTORY":
                        factories.add(new Factory(entityId, arg1, arg2, arg3)) ;
                        break;
                    case "TROOP":
                        troops.add(new Troop(entityId, arg1, arg2, arg3, arg4, arg5));
                        break;

                }
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            // Any valid action, such as "WAIT" or "MOVE source destination cyborgs"

            Optional<Factory> first = factories.stream()
                    .filter(factory -> factory.owner == 1)
                    .sorted(Comparator.comparingInt(value -> value.count))
                    .filter(factory -> factory.count > 0)
                    .findFirst();
            if (first.isPresent()){
                Factory factory = first.get();
                Optional<Link> firstLink = links.stream()
                        .filter(link -> link.source == factory.entityId)
                        .sorted(Comparator.comparingInt(value -> value.destination))
                        .filter(link -> {
                            Optional<Factory> first1 = factories.stream()
                                    .filter(factory1 -> factory1.owner == 0)
                                    .filter(factory1 -> factory1.entityId == link.destination)
                                    .findFirst();
                            return first1.isPresent();
                        })
                        .findFirst();
                if (firstLink.isPresent()){
                    Link link = firstLink.get();
                    System.out.println("MOVE "+link.source + " " + link.destination + " " + factory.count);
                }
                else {
                    System.out.println("WAIT");
                }
            }
            else {
                System.out.println("WAIT");
            }




        }
    }
}